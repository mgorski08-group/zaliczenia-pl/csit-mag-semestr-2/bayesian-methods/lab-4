class BurglaryModel:
    def __init__(self, *, p_alarm, p_bark, p_intrusion, p_a_g_i, p_b_g_i):
        self.p_alarm = p_alarm
        self.p_bark = p_bark
        self.p_intrusion = p_intrusion
        self.p_a_g_i = p_a_g_i
        self.p_b_g_i = p_b_g_i

    def calculate_p_intrusion(self, evidence):
        if evidence["alarm"] is True:
            alarm_multiplier = self.p_a_g_i / self.p_alarm
        elif evidence["alarm"] is False:
            alarm_multiplier = (1 - self.p_a_g_i) / (1 - self.p_alarm)
        elif evidence["alarm"] is None:
            alarm_multiplier = 1

        if evidence["bark"] is True:
            bark_multiplier = self.p_b_g_i / self.p_bark
        elif evidence["bark"] is False:
            bark_multiplier = (1 - self.p_b_g_i) / (1 - self.p_bark)
        elif evidence["bark"] is None:
            bark_multiplier = 1

        return self.p_intrusion * alarm_multiplier * bark_multiplier


def main():
    bm = BurglaryModel(p_alarm=0.01, p_bark=0.5, p_intrusion=0.002, p_a_g_i=0.8, p_b_g_i=0.98)

    a1b1 = bm.calculate_p_intrusion({"alarm": True, "bark": True})
    a1b0 = bm.calculate_p_intrusion({"alarm": True, "bark": False})
    a0b1 = bm.calculate_p_intrusion({"alarm": False, "bark": True})
    a0b0 = bm.calculate_p_intrusion({"alarm": False, "bark": False})
    a0bn = bm.calculate_p_intrusion({"alarm": False, "bark": None})
    a1bn = bm.calculate_p_intrusion({"alarm": True, "bark": None})
    anb0 = bm.calculate_p_intrusion({"alarm": None, "bark": False})
    anb1 = bm.calculate_p_intrusion({"alarm": None, "bark": True})
    anbn = bm.calculate_p_intrusion({"alarm": None, "bark": None})

    print(f"|             | alarm ON | alarm OFF | no evidence |")
    print(f"|    barked   | {a1b1:.6f} |  {a0b1:.6f} |   {anb1:.6f}  |")
    print(f"|  not barked | {a1b0:.6f} |  {a0b0:.6f} |   {anb0:.6f}  |")
    print(f"| no evidence | {a1bn:.6f} |  {a0bn:.6f} |   {anbn:.6f}  |")


if __name__ == '__main__':
    main()

# Q: Going back to the example, consider situation when the alarm is not silent – it blares horn
#    and flashes light when it goes off. Can computations still be performed in the same manner?
# A: In such a case the computations cannot be performed in the same manner. The model assumes
#    That the dog barking and alarm being triggered are independent events. In the case of alarm
#    blaring a horn, it would influence the dog and thus violate the assumption of independence.
